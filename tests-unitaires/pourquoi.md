
# Pourquoi ?
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->


----



## Pourquoi tester ?
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
En développement :  
 * S'assurer que ce qu'on code fonctionne
 * S'assurer que ce qu'on a déjà codé continue de fonctionner  
En maintenance :  
 * Vérifier que le bug est résolu
 * S'assurer qu'il ne se reproduira plus
 * Effets de bord / régressions ?

----

## Le test manuel, exemple
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public String recupererDebutNumeroSS(Individu monIndividu) throws MonAppliException {
      StringBuffer resultat = new StringBuffer();
      if (monIndividu != null) {
         if ("M".equals(monIndividu.getSexe())) {
            resultat.append("1");
         } else {
            resultat.append("2");
         }
         resultat.append(monIndividu.getAnneeNaissance());
         resultat.append(monIndividu.getMoisNaissance());
         resultat.append(monIndividu.getDeptNaissance());
         resultat.append(monIndividu.getArrondissementNaissance());
         resultat.append(monIndividu.getNumeroNaissance());
         resultat.append(monIndividu.getCle());
      } else {
         throw new MonAppliException("Pas d'individu");
      }
      return resultat.toString();
}
```

----

## Le test manuel, exemple
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
```Java
public static void main(String[] args) throws Exception {
      Individu monIndividu = new Individu();
      monIndividu.setAnneeNaissance("1983");
      monIndividu.setMoisNaissance("01");
      monIndividu.setDeptNaissance("75");
      monIndividu.setArrondissementNaissance("014");
      monIndividu.setCle("03");
      monIndividu.setSexe("m");
      monIndividu.setNumeroNaissance("004");
      ClasseATester maClasse = new ClasseATester();
      String retour = maClasse.recupererDebutNumeroSS(monIndividu);

      System.out.println(retour);

   }
```

----

## Le test manuel, exemple
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
![](img/manuel.png)

----

## Le test manuel, exemple
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
![](img/output.png)

----

## Le test manuel, conclusion
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 * Coûteux
 * Partiel
 * Sujet à erreurs
 * Non-collaboratif
 * Non-durable  


----


## REVENDICATIONS
 <!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->
 Qu'est ce qu'on veut ?
 * Efficacité
 * Carré
 * Cadré
 * Reproductibilité
 * Automatisation
 * Isolation
 * Durabilité
 * Collaboration  

Et quand est ce qu'on le veut ?  
**MAINTENANT !**

----

## Test "unitaire"
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

----

## Le contrat fait foi
<!-- .slide: data-state="no-toc-progress" class="no-toc-progress" -->

* **Le contrat fait foi**
* Boîte noire
* Utilisation de la Javadoc
* La puissance des interfaces

```Java
/**
	 * Calcule le numéro de sécurité sociale d'un individu
	 * @param monIndividu Individu dont on veut le numéro de sécurité sociale
	 * @return Le numéro de sécurité sociale de l'individu
	 * @throws MonAppliException Si l'individu est null
*/
	public String recupererDebutNumeroSS(Individu monIndividu) throws MonAppliException;
```
