
# 1-JUnit
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
Objectifs :
 * Créer un projet à partir de 0
 * Mettre en place JUnit
 * Tester les cas nominaux d'une méthode
 * Mutualiser le code de test
 * Documenter une méthode
 * Définir la stratégie de test à partir d'une documentation
 * Tester les exceptions d'une méthode

----

## Création d'un projet à partir de 0
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
* Créer un nouveau projet maven en choisissant “Create a simple project (skip archetype selection)” et packaging jar
* Passer en Java 8 en utilisant les properties suivantes :
```XML
<properties>
		<maven.compiler.source>1.8</maven.compiler.source>
		<maven.compiler.target>1.8</maven.compiler.target>
</properties>
```

----

## Ecriture d'une méthode à tester
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
* Créer un objet AgentService
* Ecrire une méthode isIdepValide qui vérifie que l'idep en paramètre fait exactement 6 caractères

----

## Mise en place de JUnit
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
* Ajouter la dépendance vers la dernière version de JUnit 4
* Y a-t-il de la configuration à faire ?

----

## Ecriture d'un premier test pour la méthode
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
On essayera autant que possible, dans le code d'un test, d'expliciter les 3 étapes : GIVEN, WHEN et THEN
* Ecrire un cas de test qui teste le cas OK de isIdepValide
* L'exécuter
* Ecrire un cas de test qui teste le cas NOK de isIdepValide
* L'exécuter

----

## Refactorer le code de test
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
 * Comment réduire la duplication de code ?
 * Mutualiser les comportements en utilisant une des fonctionnalités de JUnit

----

## Une méthode un poil plus compliquée
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
* Créer un object métier Agent possédant un idep
* En s'appuyant sur la méthode isIdepValide de AgentService, écrire un constructeur pour la classe Agent. Ce constructeur prendra un idep en paramètre
* Que faire si l'idep n'est pas valide ?
* Implémenter ce comportement
* Documenter le constructeur

----

## Mais confortable à tester
* Uniquement à partir de la documentation, définir les différents cas de test
* Implémenter et exécuter ces tests
