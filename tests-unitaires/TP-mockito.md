
# 3-Mockito
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
Objectifs :
 * Ajouter mockito à un projet
 * L'utiliser pour tester unitairement
 * L'utiliser pour simuler des comportements
 * Se sensibiliser à l'injection de dépendances

----

## Hello mockito
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
* Créer dans AgentService une méthode boolean agentExiste(String idep) s'appuyant sur AgentDAO.findAgentByIdep  
On veut tester cette méthode SANS tester findAgentByIdep.  
* Comment rendre agentExiste indépendant de findAgentByIdep ?
* Mettre en place un système d'injection de dépendance
* Mocker AgentDAO
* Ecrire les tests de agentExiste

----

## Mockito rebelle
<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->
* Au TP précédent, on a laissé de côté la vérification de l'SQLException de la méthode findAgentByIdep(String idep)
* Comment tester ?
* Utiliser l'injection de dépendances pour pouvoir injecter l'object Connection
* Créer un object Connection piégé
* L'injecter et écrire le test
