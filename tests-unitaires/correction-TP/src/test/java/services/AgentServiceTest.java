package services;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import services.AgentService;

public class AgentServiceTest {

	AgentService agentService;
	
	@Before
	public void before() {
		agentService = new AgentService();
	}
	
	@Test
	public void testIsIdepValideOK() throws Exception {
		//GIVEN
		String idep = "ABCDEF";
		
		//WHEN
		boolean ok = agentService.isIdepValide(idep);
		
		//THEN
		assertTrue(ok);
	}
	
	@Test
	public void testIsIdepValideNOK() throws Exception {
		//GIVEN
		String idep = "AB";
		
		//WHEN
		boolean ok = agentService.isIdepValide(idep);
		
		//THEN
		assertFalse(ok);
	}

}
