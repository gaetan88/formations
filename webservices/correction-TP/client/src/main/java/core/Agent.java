package core;

public class Agent {

	public static final int CATEGORIE_A = 1, CATEGORIE_B = 2, CATEGORIE_C = 3;
	String idep;
	int categorie;
	
	public String getIdep() {
		return idep;
	}
	public void setIdep(String idep) {
		this.idep = idep;
	}
	public int getCategorie() {
		return categorie;
	}
	public void setCategorie(int categorie) {
		this.categorie = categorie;
	}
	@Override
	public String toString() {
		return "Agent [idep=" + idep + ", categorie=" + categorie + "]";
	}
	
	
	
}
