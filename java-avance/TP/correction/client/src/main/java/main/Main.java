package main;

import java.util.List;

import core.Individu;
import csvparser.CSVParser;
import csvparser.UltimateCSVParser;

public class Main {

	public static void main(String[] args) {
		CSVParser<Individu> parser = new UltimateCSVParser<Individu>();
		List<Individu> individus = parser.parseStream(Main.class.getResourceAsStream("/individu.csv"));
		System.out.println(individus.size());
	}

}
