package csvparser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class UltimateCSVParser implements CSVParser {

	@Override
	public List<String[]> parseStream(InputStream stream) {
		List<String[]> liste = new ArrayList<String[]>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
			String line = null;
			String header = reader.readLine();
			handleHeader(header);
			while ((line = reader.readLine()) != null) {
				String[] object = handleLine(line);
				liste.add(object);
			}
		}
		catch (IOException e) {
			
		}
		
		return liste;
	}
	
	private void handleHeader(String header) {
		
	}
	
	private String[] handleLine(String line) {
		return line.split(";");
	}

}
