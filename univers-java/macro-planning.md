<!-- .slide: data-background-image="images/spring.png" data-background-size="1200px" class="chapter" -->
## Macro planning





### Jour 1 : Basics
<!-- .slide: class="slide" -->  
| 1/4 journée | Type        | Notions                                                      |
| ---------- | -------------------- | ----------------------------------------------------------------|
| 1              | Cours | Concept, historique, syntaxe, maven, packaging                          |
| 2              | TP | Hello world, ?                                  |
| 3       | Cours |                                                            |
| 4      | TP |                                                            |





### Jour 2 : JEE
<!-- .slide: class="slide" -->
| 1/4 journée | Type        | Notions                                                      |
| ---------- | -------------------- | ----------------------------------------------------------------|
| 1              | Cours | Servlets, JSP, JAX-RS, war                         |
| 2              | TP | Webservice                                  |
| 3       | Cours |                                                           |
| 4      | TP |                                                            |





### Jour 3 : Foire aux frameworks
<!-- .slide: class="slide" -->
| 1/4 journée | Type        | Notions                                                      |
| ---------- | -------------------- | ----------------------------------------------------------------|
| 1              | Cours | JDBC                          |
| 2              | TP | Hello world, ?                                  |
| 3       | Cours | Hibernate                                                          |
| 4      | TP |                                                            |





### Jour 4 : Android
<!-- .slide: class="slide" -->
| 1/4 journée | Type        | Notions                                                      |
| ---------- | -------------------- | ----------------------------------------------------------------|
| 1              | Cours | Spécificités du dev mobile                         |
| 2              | Cours | Dev Android                                  |
| 3       | TP |                                                           |
| 4      | TP |                                                            |






### Jour 5 : Android
<!-- .slide: class="slide" -->
| 1/4 journée | Type        | Notions                                                      |
| ---------- | -------------------- | ----------------------------------------------------------------|
| 1              | Cours | Concepts avancés         |
| 2              | TP | TP avancé                             |
| 3       | Cours |    Kotlin                                                       |
| 4      | TP |     TP kotlin                                                      |






### Jour 6 : Projet
<!-- .slide: class="slide" -->
| 1/4 journée | Type        | Notions                                                      |
| ---------- | -------------------- | ----------------------------------------------------------------|
| 1              | Projet |                          |
| 2              | Projet |                                   |
| 3       | Projet |                                                           |
| 4      | Projet |                                                            |








### Jour 7 : Projet
<!-- .slide: class="slide" -->
| 1/4 journée | Type        | Notions                                                      |
| ---------- | -------------------- | ----------------------------------------------------------------|
| 1              | Projet |                          |
| 2              | Projet |                                   |
| 3       | Projet |                                                           |
| 4      | Projet |                                                            |
